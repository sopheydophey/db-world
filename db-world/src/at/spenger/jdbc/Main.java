package at.spenger.jdbc;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Main {
	
	public static void main(String[] args){
		Connection con = null;
		
		String url = "jdbc:mysql://localhost:3306/world";
		String user = "root";
		String password = "";
		
		try {
			con = DriverManager.getConnection(url, user, password);
			DatabaseMetaData dbMetaData = con.getMetaData();
			String productName = dbMetaData.getDatabaseProductName();
			System.out.println("Database: " + productName);
			String productVersion = dbMetaData.getDatabaseProductVersion();
			System.out.println("Version: " + productVersion);
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from world.city");
			
			int max = rs.getMetaData().getColumnCount();
			for(int i=1; i <= max; i++){
				System.out.print(rs.getMetaData().getColumnLabel(i) + "|"); 
			}
			
			System.out.println();
			
			while(rs.next()){
				for(int i=1; i <= max; i++){
					System.out.print(rs.getString(i) + "|");
				}
				System.out.println();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
